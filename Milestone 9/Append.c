#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
}; 
  
void Append(struct List** a_ref, struct List** b_ref){
    struct List* current;
    
    if(*a_ref == NULL){
        *a_ref = *b_ref;
    }
    else{
        current = *a_ref;
        while(current->next !=NULL){
            current = current->next;
        }
        
        current->next = *b_ref;
    }
    
    *b_ref = NULL;
}
  
void push(struct List** b_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*b_ref); 
    (*b_ref)    = new_node; 
} 
  
int main() 
{ 
    struct List* b = NULL; 
    struct List* a = NULL;
    push(&b, 5); 
    push(&b, 2); 
    push(&b, 1);  
    push(&b, 3); 
    push(&b, 5);
    push(&b, 5);
     
    Append(&a,&b);
     
    printf("b is appended at the end of a" ); 
} 