#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int id;
    char name[20];
    int marks;
} Student;

int compareMarks(const void* a, const void* b) {
    const Student *p = a;
    const Student *q = b;

    return q->marks - p->marks;
}

int compareInt(const void* a, const void* b){
    const int *p = a;
    const int *q = b;
    return *p - *q;
}

int compareStr(const void* a, const void* b){
    return strcmp(a, b);
}



void swap(void* a, void* b, size_t len)
{
    unsigned char * p = a, * q = b, tmp;
    for (size_t i = 0; i != len; ++i)
    {
        tmp = p[i];
        p[i] = q[i];
        q[i] = tmp;
    }
}

void bubbleSort(void* arr, int n, size_t size, int (*compare)(const void*, const void*)) {
    size_t i, j;

    for (i = 0; i < size*n; i =i+size){
        for (j = 0; j < size*(n-1); j=j+size) {
            if (compare(arr+i, arr+j) < 0){
                swap(arr+i, arr+j, size);
            }
        }
    }

}

int main() {

    // Sorting Integer Array
    int A[] = {3, 5, 8, 7, 9, 1, 3};
    int n = sizeof(A)/sizeof(int);
    bubbleSort(A, n, sizeof(int), compareInt);

    for (int i = 0; i < n; i++)
        printf("%d ", A[i]);
    printf("\n\n");

    // Sorting String Array
    char names[][20] = {"Ramesh", "Ashish", "Sahil", "Rahul", "Varun", "Onkar"};
    n = sizeof(names)/sizeof(names[0]);

    bubbleSort(names, n, sizeof(names[0]), compareStr);

    for (int i = 0; i < n; i++)
        printf("%s ", names[i]);
    printf("\n\n");

    // Sorting Student(structure) Array
    Student students[] = {
                            {1, "Onkar", 12},
                            {2, "Pranit", 15},
                            {3, "Ashish", 15},
                            {4, "Prathamesh", 12},
                            {5, "Rishi", 14},
                            {6, "Rohit", 12},
                        };
    n = sizeof(students)/sizeof(Student);
    bubbleSort(students, n, sizeof(Student), compareMarks);

    for (int i = 0 ; i < n; i++) {
        printf("%d %s %d\n", students[i].id, students[i].name, students[i].marks);
    }
}
