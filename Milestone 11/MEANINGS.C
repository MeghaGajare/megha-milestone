#include <stdio.h>
#include <string.h>

int main()
{
		FILE *fp = fopen("Dictionary.txt", "r");
		const char s[2] = ":";
		char *token;
		int i;
		if(fp != NULL)
		{
				char line[100];
				while(fgets(line, sizeof line, fp) != NULL)
				{
						token = strtok(line, s);
						for(i=0;i<2;i++)
						{
								if(i==0)
								{
										printf("Word: %s\n",token);
										token = strtok(NULL,s);
								} else
								{
										printf("Meaning: %s\n\n",token);
								}
						}
				}
				fclose(fp);
		} else {
				perror("meanings.txt");
		}
		return 0;
}